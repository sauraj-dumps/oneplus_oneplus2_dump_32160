#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_oneplus2.mk

COMMON_LUNCH_CHOICES := \
    lineage_oneplus2-user \
    lineage_oneplus2-userdebug \
    lineage_oneplus2-eng
