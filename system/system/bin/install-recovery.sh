#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/soc.0/f9824900.sdhci/by-name/recovery:20549632:28517f000fe286dfa385bf132e68cef06c9bef51; then
  applypatch --bonus /system/etc/recovery-resource.dat \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/soc.0/f9824900.sdhci/by-name/boot:11251712:a3bc8de7a91b769e18931db5cc0401ddb89b00cb \
          --target EMMC:/dev/block/platform/soc.0/f9824900.sdhci/by-name/recovery:20549632:28517f000fe286dfa385bf132e68cef06c9bef51 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
